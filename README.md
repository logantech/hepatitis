# Hepatitis

* Start with a [UCI dataset](https://archive.ics.uci.edu/ml/datasets/Hepatitis) enumerating 155 patient outcomes with associated clinical metrics related to hepatitis. Import and clean data, examine correlations, perform feature selection & engineering, and create multiple predictive models. Evaluate models for overall performance.

Quickstart:

* Download Python 3 notebooks: LoganDowning-M02-Dataset.ipynb, LoganDowning-M03-DataModel.ipynb
* Download datasource: hepatitis.csv
* Open each notebook using Jupyter Notebook

Or view these [screen grabs](screenshots/) of the entire notebooks.
___

Compute bivariate correlations between each feature.

![Correlation matrix](images/corr.png)

Examine distributions of some features to establish statistical baselines.

![Distribution plot of the age feature](images/distribution.png)

Scale and transform features to a common range.

![Distribution plots of multiple features](images/feature-scaling.png)

Exploratory data analysis. Generate bivariate analyses of numeric features, sharded by outcome, to detect patterns and learn feature importances.

![16 pairplots showing the pairwise correlation of each set of features](images/bivariate-analysis.png)

Create a machine learning model based on training data, and apply to test data. In this case we create a Support Vector Machine model.

![Two pie charts showing outcomes for each class](images/survival-outcomes.png)



Examine the receiver operating characteristic curve for the SVM model to establish sensitivity and measure AUC.

![Receiver operating characteristic curve (ROC)](images/roc.png)



Examine the performance of multiple machine learning model approaches (SVM and Naive Bayes classifiers) trained on the same set of training data and evaluated against common test data.

![Barplots showing the performance of two model approaches for a single class outcome](images/class-performance.png)
